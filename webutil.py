# Assorted utility code for used with web.py

import web, mimetypes
web.webapi.internalerror = web.debugerror

import re

from breve import Template
from breve.tags.html import tags as T
from breve.tags.html import xml
from breve.flatten import flatten

space = xml("&nbsp;")
Template.debug = True

def template(name, **kwargs):
    """Apply the breve template `name' over template args in
    kwargs.

    `kwargs' can also contain
      - the arguments to Template class.
      - `content_type'
    """
    vars = {}
    vars.update(kwargs)

    content_type = kwargs.get("content_type", "text/html")
    web.header("Content-Type","%s; charset=utf-8" % content_type)

    if type(name) is str:
        init_kwargs = dict(tags=T, root="breve/")
        init_kwargs.update(kwargs)
        tmpl = Template(**init_kwargs)
        print tmpl.render(template=name, vars=vars)
    else:
        print flatten(name)

class static:
    "serve static files"
    def GET(self, name):
        try:
            filename = "./static/%s" % name
            filetype = mimetypes.guess_type(filename)[0] or \
                'application/octet-stream'
            web.header("Content-type", filetype)
            print open(filename, "rb").read()
        except IOError:
            web.notfound()
            raise
 

# BeautifulSoup oddities
# http://mail.python.org/pipermail/python-list/2006-December/420175.html
from htmlentitydefs import name2codepoint
name2codepoint = name2codepoint.copy()
name2codepoint['apos']=ord("'")
EntityPattern = re.compile('&(?:#(\d+)|(?:#x([\da-fA-F]+))|([a-zA-Z]+));')

def decode_entities(s, encoding='utf-8'): 
    def unescape(match):
	code = match.group(1)
        if code:
            return unichr(int(code, 10))
        else:
            code = match.group(2)
            if code:
                return unichr(int(code, 16))
	    else:
                return unichr(name2codepoint[match.group(3)])
    return EntityPattern.sub(unescape, s)
