// max,min 'age' of reddit links
// 'age' can be found in the returned json (reddit api)
var max_age = null;
var min_age = null;
var s;
var slider_value_backup = null;

function read_slider(){
    return s.getValue();
}

function write_slider(secs){
    s.setValue(secs);
}


function init(){
    // slider
    roundElement('duration', {'color': 'rgb(230,230,230);'});
    s = new Slider($("duration-slider"),
		       $("duration-slider-input"));

    reload_all(false);
    
    s.onchange = function() {
	emphasize_links(false);
    }

}

function save_last_visited(){
    expires_hrs = parseInt((max_age - min_age)/(60*60));
    var date = new Date();
    createCookie("lastVisited", parseInt(date.getTime()/1000), expires_hrs);
}

function retrieve_age(){
    var date = new Date();
    var last_visited = readCookie("lastVisited");
    logDebug("read cookie: " + last_visited);
    if (last_visited != null){
	since = parseInt(date.getTime()/1000) - parseInt(last_visited);
	write_slider(since);
    }else{
	// no cookie; set to max
	write_slider(max_age);
    }
}

function emphasize_links(scroll_to_new){
    var chosen_age = read_slider();
    var ago_desc = "from " + human_readable_time(chosen_age) + " ago";
    $("ago").innerHTML = ago_desc;
    document.title = "reddit cloud - " + ago_desc;
    
    links = getElementsByTagAndClassName("a", "reddit_link");
    var first = true;

    map(function(link){
	    var age = getNodeAttribute(link, "data:age");
	    age = parseInt(age);

	    if (age > chosen_age){
                // bury this link
		swapElementClass(link, "transOFF", "transON");
            }
	    else{
                swapElementClass(link, "transON", "transOFF");
		if (first){
		    // /reddit/#new goes to the first active A
                    link.name = "new"; 
                    first = false;
                }
            }
	    link.name = "";
	},
        links);

    if (scroll_to_new)
	window.location = "#new";
}

function reload_all(preserve_slider){
    slider_value_backup = read_slider();
    // TODO: code smell below!
    max_age = -1;
    min_age = 9999999;
    function post_load(){
	if (preserve_slider)
	    write_slider(slider_value_backup);
	else
	    retrieve_age();
	save_last_visited();
    }
    d = reload_reddit("").addCallback(reload_reddit, "programming"
				      ).addCallback(reload_reddit, "science"
						    ).addCallback(post_load);
}

function reqError(e){ alert("request error: " + e); }
function reload_reddit(sub){

    var gotData = function(data){
	try{
	    var node = draw_reddit(sub, data);
	    swapDOM("loading", node);
	 
	    // max,min age
	    var ages = map(function(l){ return l['age']==null?0:l['age']; },
			   data);
	    if (max_age < listMax(ages)) max_age = listMax(ages);
	    if (min_age > listMin(ages)) min_age = listMin(ages);
	    s.setMaximum(max_age);
	    s.setMinimum(0); // must be 0, otherwise s.getValue()....
	    retrieve_age();
	    emphasize_links(true);
	}catch(e){
	    // Mochikit and FireBug do not catch this, dunno why.
	    logError(e);
	    alert("error: "+e);
	}
    }

    //var loading = swapDOM("reddit."+sub, DIV({'id':'loading'}, "foo"));
    /* FIXME: This statement causes `script error' in IE */
    var loading = swapDOM("reddit."+sub,
		          DIV({'id':'loading'},
			       create_bar(A(null,
				            "Loading " + sub + " ",
				            IMG({'src':"static/ajax-loader.gif"},
					    '')))));

    var url = "api/hot?sub="+sub;
    logDebug("loadJSONDoc: " + url);
    return loadJSONDoc(url).addCallbacks(gotData, reqError);
}

function draw_reddit(sub, links){
    var SUP = createDOMFunc("sup");

    // the "tag cloud" thingy
    var size_list = function(values, min_size, max_size, inc_size, def){
	total_size = (max_size - min_size) / inc_size;
	max_value = listMax(values);
	var sizes = new Array(values.length);
	for (i=0; i<values.length; ++i){
	    sizes[i] = def;
	    if (values[i])
		sizes[i] = min_size + (inc_size *
				       ((values[i] * total_size)/max_value));
	}
	return sizes;
    }
    
    render_link = function(l,size){
        var ago = null;
	if (l['age'] == null)
            ago = "- secs";
	else
            ago = human_readable_time(l['age']);

	tooltip = ((l['score']==null)?"-":l['score']) + " points. " + ago +
	          " ago."
	return SPAN({'class':'link'},
		     SUP(null,
                         A({'class': 'comments_link',
			   'href': l['comments_href'],
			   'title': l['comments']+" Comments in reddit"},
			  l['comments'])),
		     " ",
		     A({'class': 'reddit_link transOFF',
		       'href': l['href'],
		       'title': tooltip,
		       'style': 'font-size: '+size+'px;',
		       'data:age': l['age']}, l["title"])
		    );
    }

    var scores = map( itemgetter("score"), links );
    var sizes = size_list(scores, 12, 36, 2, 16);

    var url = "http://reddit.com";
    var h2 = "reddit.com";
    if (sub != ""){
        url = "http://" + sub + ".reddit.com";
        h2 = sub + ".reddit.com";
    }

    var node = DIV({'id': "reddit."+sub, 'style': 'margin-bottom: 30px;'},
	       create_bar(A({'href': url}, h2)),
	       DIV(null, map(render_link, links, sizes)));

    return node;
}

function create_bar(child){
    var bar = H2({'class': 'bar'}, child);
    roundElement(bar, {'color': 'gray'});
    return bar;
}

function human_readable_time(seconds){
    var days, hrs, mins, secs;
    secs = seconds;
    mins = parseInt(secs/60);
    hrs = parseInt(secs/(60*60));
    days = parseInt(secs/(24*60*60));

    if (days > 0){
	hrs1 = hrs % 24
	if (hrs1 > 0){
	    if (days > 1) return days + " days " + hrs1 + " hrs";
	    else return "1 day " + hrs1 + " hrs";
	}
        else{
	    return days + ((days > 1)?" days":" day");
	}
    }

    if (hrs > 0){
	mins1 = mins % 60
	if (mins1 > 0)
	    return hrs + " hrs " + mins1 + " mins";
        else
	    return hrs + " hrs";
    }

    if (mins > 0)
	return mins + " mins";
    else
	return secs + " secs";
}

// Cookie functions from, 
// http://www.quirksmode.org/js/cookies.html

function createCookie(name,value,hrs) {
	if (hrs) {
		var date = new Date();
		date.setTime(date.getTime()+(hrs*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
	logDebug("cookie-store: " + document.cookie);
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0)
		    return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
