# Run this with nose

import reddit

url = "http://reddit.com/"

def non_empty_string(s):
    return type(s) in [str, unicode] and len(s.strip()) > 0


def test_url_get():
    val = reddit.get_url(url)
    assert type(val) is str and len(val) > 0

def test_reddit_links():
    r = reddit.Reddit(url)
    for link in r.links:
        valid_link(link)

def test_guess_time():
    cases = [
        ("21 hours", 21*60*60),
        ("7 Days 21 hours", 7*24*60*60 + 21*60*60),
        ("1 day", 24*60*60),
        ]
    for s,t in cases:
        g = reddit.guess_time(s)
        assert type(g) is int, "Must return time in seconds"
        assert g == t, \
            "guess_time(s) returned %d, while expected %d" % (g, t)

        
def valid_link(l):
    assert non_empty_string(l.href)
    assert non_empty_string(l.title)
    assert non_empty_string(l.user)
    assert non_empty_string(l.comments_href)
    assert type(l.comments) is int
    assert type(l.score) is int or l.score is None
