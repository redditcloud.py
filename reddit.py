# alternate view for reddit
# (and rewrite of reddit.ss, the tag cloud)
#
# TODO: insert feature list

# -*- mode: python -*-

import web, webutil
import os, time, logging, re
import pickle, urllib2, simplejson

from breve.tags.html import tags as T
from breve.tags.html import xml
from BeautifulSoup import BeautifulSoup


LOG_FILE      = "/home/protected/logs/reddit.log"
CACHE         = "/home/protected/data/reddit.cache"
CACHE_TIMEOUT = 60*5 # 5 mins

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename=LOG_FILE)

def now():
    return long(time.time())

def cached(func):
    """Decorator that caches the return value to disk. Cache is invalidated
    after `CACHE_TIMEOUT' seconds.

    See global variable `CACHE'
    """
    def cached_func(*args):
        if not os.path.exists(CACHE):
            pickle.dump({}, file(CACHE, 'w'))
        cache = pickle.load(file(CACHE))
        # XXX: this must be like func.full_path_name
        key = (func.func_name, args)
        if key in cache:
            t, val = cache.get(key)
            if now()-t < CACHE_TIMEOUT:
                return val
        val = func(*args)
        t = now()
        cache[key] = (t, val)
        pickle.dump(cache, file(CACHE, 'w'))
        return val

    return cached_func

@cached
def get_url(url):
    return urllib2.urlopen(url).read()

def short_url(full_url):
    """
    >>> print short_url("http://reddit.com/")
    reddit.com
    >>>
    """
    return full_url[7:].rstrip("/")


time_patterns = {
      "hrs": re.compile('(\d+) hours'),
      "days_hrs": re.compile('(\d+) Days (\d+) hours'),
      "days": re.compile('(\d+) days'),
      "1 day": re.compile('1 day')
    }
def guess_time(human_readable):
    """
    >>> guess_time("7 Days 21 hours")

    See test.py for more rigorous tests.
    """
    def mpat(k): return time_patterns[k].match(human_readable)
    
    m = mpat("hrs")
    if m:
        return int(m.group(1)) * 60*60

    m = mpat("days_hrs")
    if m:
        return int(m.group(1))*24*60*60 + \
               int(m.group(2))*60*60

    m = mpat("days")
    if m:
        return int(m.group(1))*24*60*60
    
    if mpat("1 day"):
        return 24*60*60

    logging.warn("Cannot guess time for: %s", human_readable)
    
            
class RedditLink(web.Storage):
    "See `__str__'"

    def __str__(self):
        return "<RedditLink> %s\n  (%s)\n  by %s (%s ago). " + \
            "%d points (#%d comments) at %s\n  Rank: %d %s" % \
            (self.title, self.href, self.user,
             self.age, self.score or -1, self.comments, self.comments_href,
             self.rank, self.top and "*" or "")


class Reddit:

    def __init__(self, url):
        self.url = url
        self.links = []
        self._parse()

    def _parse(self):
        logging.debug("Parsing %s", self.url)
        soup = BeautifulSoup(get_url(self.url))

        def parse_link_html(a, b):
            # `a' is the first tr, `b' is the second
            # both render a complete reddit link
            l = RedditLink()

            rank = a.td.span
            if rank:
                l.rank = int(rank.a.string.strip()[:-1]) # get rid of .
                l.top = True
            else:
                l.rank = int(a.td.string.strip()[:-1])
                l.top = False
                
            title_a = a.find("td", colspan="3").find("a", )
            l.title = webutil.decode_entities(title_a.string.strip())
            l.href  = title_a["href"].encode("utf-8")
            l.href = l.href.replace("&amp;", "&") #BeautifulSoup madness

            l.user = b.td.a.string.strip().encode("utf-8")
            if b.td.span is None:
                # no score shown
                l.score = None
            else:
                n, s = b.td.span.string.strip().encode("utf-8").split()
                l.score = int(n)

            l.age = b.td.contents[2].strip().encode("utf-8")
            l.age = " ".join(l.age.split()[1:3])
            if l.age == "":
                l.age = None
            else:
                l.age = guess_time(l.age)

            l.comments_href = b.td.a.findNext("a")[
                   "href"].strip().encode("utf-8")
            if not l.comments_href.startswith("http://"):
                l.comments_href = self.url + l.comments_href
                
            c = b.td.a.findNext("a").string.strip()
            if c == "comment":
                l.comments = 0
            else:
                l.comments = int(b.td.a.findNext("a").string.strip().split()[0])

            return l
                    
        table = soup.find("table", id="siteTable")
        tr = table.find("tr")
        while True:
            tr = a = tr.findNext('tr', attrs={"class": ["evenRow", "oddRow"]})
            if tr is None: break
            tr = b = tr.findNext('tr', attrs={"class": ["evenRow", "oddRow"]})
            self.links.append(parse_link_html(a, b))


class api:
    "the reddit api"
    GET = web.autodelegate('GET_')

    def GET_hot(self):
        "Get hot links for subreddit `sub'"
        #web.header("Content-Type","%s; charset=utf-8" % "text/x-json")
        sub = web.input(sub="").sub
        r = Reddit(self.reddit_url_for(sub))
        print simplejson.dumps(r.links)

    def GET_(self):
        webutil.template(["Only one method at the moment: ",
                          T.code["/api/hot?sub=science"]])

    def reddit_url_for(self, sub):
        if sub == "":
            return "http://reddit.com"
        else:
            assert sub in ("programming", "science")
            return "http://%s.reddit.com" % sub

# HTTP
######


urls = (
    '/static/(.*)', 'webutil.static',
    '/sheep',       'sheep',
    '/',            'index',
    '/api/(.*)',    'api',
    '/api',         'redirect api/',
)

class index:
    def GET(self):
        webutil.template("index")

class sheep:
    def GET(self):
        webutil.template([T.head[
                            T.link (rel="stylesheet", href="static/reddit.css"),
                          ],
                          T.body[
                            T.div(class_="transON")["I am a div"],
                            T.span(class_="transON")["I am a span"],
                            T.a(class_="transON")["I am a A"],"next",
                          ],
                         ])

logging.info("Module loaded")
