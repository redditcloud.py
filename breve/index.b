# -*- mode: python -*-

html [
    head [
        title [ 'reddit cloud' ],
        meta ( name = "description", content = "reddit.com alternate view" ),
        link ( rel="stylesheet", href="static/reddit.css"),
        link ( rel="stylesheet", href="static/slider/bluecurve.css"),
        script ( type="text/javascript", src="static/MochiKit.js"),
        script ( type="text/javascript", src="static/slider/packed.js"),
        script ( type="text/javascript", src="static/reddit.js"),
	#script ( type="text/javascript", src="/static/firebug/firebug.js"),
    ],

    body (onload="init();")[
        p [
           "Welcome to the ", b["reddit cloud"],
        ],

        div(class_="floatRight", id="duration")[
            div(align="center")[
                xml("&nbsp;"),
                b["Emphasize"],
                " hot links posted..",
                xml("&nbsp;")
                ],
            div(class_="slider", id="duration-slider",
                style="margin: 0 10 0 10;",
                tabIndex="1")[
                input(class_="slider-input", id="duration-slider-input",
                      name="duration-slider-input")
            ],
            div(id="ago", align="center")["from the beginning"],
        ],
        
        p[
            "This is an alternate view for reddit that I am experimenting ",
            "with. There is also a ", a(href="api/")["JSON API"],
            ". ",
            "Feel free to share your thoughts with ",
            a(href="http://nearfar.org/")["me"], ". You can find the ",
            "Python source code ",
            a(href="http://nearfar.org/code/reddit/")["here"],
            ". ", tt["Emphasize"], " feature is known not to work in IE."
        ],
        
        div[ a(href="javascript:reload_all(false);")["reload"]],

        div(id="reddit.all")[
            div(id="reddit."),
            div(id="reddit.programming"),
            div(id="reddit.science"),
        ],

        br,
    ]
]
